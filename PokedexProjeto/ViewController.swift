//
//  ViewController.swift
//  PokedexProjeto
//
//  Created by COTEMIG on 20/07/43 AH.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonHabilidades: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonHabilidades.isEnabled = false
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(clickNoLabel))
        
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tapGesture)
    }

    @objc func clickNoLabel(){
        print("clicou no label")
    }
    
    @IBAction func clickPokedex(_ sender: Any) {
        titleLabel.text = "CLicou no pokedex"
        titleLabel.textColor = .cyan
        buttonHabilidades.isEnabled = true
    
    }
    
    
    @IBAction func clickMovimentos(_ sender: Any) {
        print("Clicou em movimetos")
    }
}



